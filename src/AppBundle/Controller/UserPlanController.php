<?php 
namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\UserPlan;
use MediaBundle\Entity\Media;
use UserBundle\Entity\User;
use AppBundle\Form\UserPlanType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Razorpay\Api\Api;
class UserPlanController extends Controller
{






    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $packageplan =   $em->getRepository("AppBundle:UserPlan")->findBy(array(),array("id"=>"asc"));
        return $this->render("AppBundle:UserPlan:index.html.twig",array("plans"=>$packageplan));
    }





    public function api_OrderAction(Request $request,$token)
    {
        if ($token!=$this->container->getParameter('token_app')) {
            throw new NotFoundHttpException("Page not found");  
        }
        $orderid=$request->get("orderid");
        $amount=$request->get("amount");
        $imagineCacheManager = $this->get('liip_imagine.cache.manager');
        $api = new Api('rzp_live_JGQtGTCo71rmfO','vHwCAGSyzdBPAJP6PbEy9o6L');
        $order  = $api->order->create(array('receipt' => $orderid, 'amount' => $amount, 'currency' => 'INR')); 
                
        
     
                $code="200";
                $errors=array();
                $errors[]=array("name"=>"orderid","value"=> $order['id']);
      
                $message=" Date Value";
                $error=array(
                    "code"=>$code,
                    "message"=>$message,
                    "values"=>$errors
                    );
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent=$serializer->serialize($error, 'json');
        return new Response($jsonContent);
    }



















    public function api_addPLanAction(Request $request,$token)
    {
        if ($token!=$this->container->getParameter('token_app')) {
            throw new NotFoundHttpException("Page not found");  
        }
        $userid=$request->get("username");
        $plantype=$request->get("plantype");
        $txnid=$request->get("txnid");
       



        $imagineCacheManager = $this->get('liip_imagine.cache.manager');


        // $api = new Api('rzp_test_SPQREEJ2wr2xZH','A7pfGaGPlT8TnOC4ZUi7bd4D');
        // $payment = $api->payment->fetch($txnid);
        $url = "https://api.razorpay.com/v1/payments/" . $txnid;
        $key = "rzp_live_JGQtGTCo71rmfO";
        $secret = "vHwCAGSyzdBPAJP6PbEy9o6L";
        $payment=$this->getData($url,$key,$secret);

       
        $errors=array();
        

        $em = $this->getDoctrine()->getManager();
        $u=$em->getRepository('AppBundle:UserPlan')->findOneByTxnid($txnid);
        $user=$em->getRepository('UserBundle:User')->findOneByUsername($userid);
        if($u==null)
        {      
           
                $am=$payment["amount"]/100;
                
                $userplan = new UserPlan();
                $userplan->setUserName($userid);
                $userplan->setPlanType((int)$plantype);
                $userplan->setName($user->getName());
                $userplan->setAmount($am);
                $userplan->setTxnId($txnid);
                $userplan->setOrderid($payment["order_id"]);
                $userplan->setStartDate(new \DateTime());
                $userplan->setEndDate(new \DateTime('now +'.$plantype.' day'));
                $userplan->setStatus(1);
                
                $em->persist($userplan);
                $em->flush();
                $code=200;
                $message="You have successfully subscribed plan!!!!";
                $errors[]=array("name"=>"id","value"=>$userplan->getId());
                $errors[]=array("name"=>"txnid","value"=>$userplan->getTxnId());
                $errors[]=array("name"=>"enddate","value"=>$userplan->getEndDate()->format('d-m-Y'));

        }
        else
         {
            $code="500";
            $message="Not valid Credential!!!"; 
         }

           
                $error=array(
                    "code"=>$code,
                    "message"=>$message,
                    "values"=>$errors
                    );
                $encoders = array(new XmlEncoder(), new JsonEncoder());
                $normalizers = array(new ObjectNormalizer());
                $serializer = new Serializer($normalizers, $encoders);
                $jsonContent=$serializer->serialize($error, 'json');
                return new Response($jsonContent);
    }



    public function api_checkAction(Request $request,$token)
    {
        if ($token!=$this->container->getParameter('token_app')) {
            throw new NotFoundHttpException("Page not found");  
        }
        $userid=$request->get("username");
        $errors=array();
        

        $em = $this->getDoctrine()->getManager();
        $u=$em->getRepository('AppBundle:UserPlan')->findOneBy([
            'username' => $userid,
            'status' => 1,]);
        
        if( $u!=null)
        {
        $end = new \DateTime();
       
       
        // $interval = var_dump(date_diff($end-$u->getEndDate()));
        // $errors[]=array("name"=>"now","value"=>$interval->days);
        $errors[]=array("name"=>"plan","value"=>$u->getPlanType());
        $errors[]=array("name"=>"enddate","value"=>$u->getEndDate()->format('d-m-Y'));
        if($u->getEndDate()>$end)
        {
            $code="200";
            $message="Subscription Active";
        }
        else {


            $u->setStatus(0);
            $em->persist($u);
            $em->flush();
            $code="500";
            $message="Subscription Expired";
        }
        }
        else 
        {
            $code="500";
            $message="";
        }
        
       
        $error=array(
            "code"=>$code,
            "message"=>$message,
            "values"=>$errors
            );
       
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent=$serializer->serialize($error, 'json');
        return new Response($jsonContent);
    }


    public function editAction(Request $request,$id)
    {
        $em=$this->getDoctrine()->getManager();
        
        $user=$em->getRepository('UserBundle:User')->find($id);
        if ($user==null) {
            throw new NotFoundHttpException("Page not found");
        }
       
        $u=$em->getRepository('AppBundle:UserPlan')->findOneBy([
            'username' => $user->getUsername(),
            'status' => 1]);
      
        if($u!=null)
        {
            $this->addFlash('success', 'User have alreay active plan');
            $packageplan =   $em->getRepository("AppBundle:UserPlan")->findBy(array(),array("id"=>"asc"));
            return $this->render("AppBundle:UserPlan:index.html.twig",array("plans"=>$packageplan));



        }
        else 
        {
            
        $val=new \DateTime();
        $userplan = new UserPlan();
        $userplan->setUserName($user->getUsername());
        $userplan->setName($user->getName());
        $userplan->setTxnId("TxnidManual_".$val->format('d-m-Y'));
        $userplan->setOrderid("OrderManual".$val->format('d-m-Y'));
        $userplan->setStartDate(new \DateTime());
        $userplan->setStatus(1);
        $form = $this->createForm(new UserPlanType(),$userplan);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {


           if($userplan->getPlanType()=="1")
           {
            $plans =   $em->getRepository("AppBundle:PackagePlan")->findOneBy(['type' => "Daily"]);
           }
           else if($userplan->getPlanType()=="30")
           {
            $plans =   $em->getRepository("AppBundle:PackagePlan")->findOneBy(['type' =>"Monthly"]);  
           }
           else 
           {
            $plans =   $em->getRepository("AppBundle:PackagePlan")->findOneBy(['type' => "Annualy"]); 
           }








             $userplan->setAmount($plans->getAmount());
            $userplan->setEndDate(new \DateTime('now +'.$userplan->getPlanType().' day'));
            $em->persist($userplan);
            $em->flush();
            $this->addFlash('success', 'Operation has been done successfully');
            return $this->redirect($this->generateUrl('app_Subscribers_index'));
        }
       
        
        return $this->render("AppBundle:UserPlan:edit.html.twig",array("plan"=>$userplan,"form"=>$form->createView()));
       }
    }











    function getData($url, $key, $secret) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, $key . ":" . $secret);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($response, true);
        return $data;
    }
}
?>