<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use MediaBundle\Entity\Media;
/**
 * Actor
 *
 * @ORM\Table(name="package_plan")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PackagePlanRepository")
 */
class PackagePlan
{
   
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 3,
     *      max = 25,
     * )
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(name="amount	", type="string", length=255)
     */
    private $amount	;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at	",type="datetime")
     */
     private $created_at	;

   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }



     /**
     * Set name
     *
     * @param string $created_at
     * @return $this
     */
     public function setcreated_at($date)
     {
         $this->created_at = $date;
 
         return $this;
     }


     /**
     * Get createdat
     *
     * @return \DateTime 
     */
     public function getcreated_at()
     {
         return $this->created_at;
     }



    /**
     * Set name
     *
     * @param string $name
     * @return PackagePlan
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

   

  
  
  
    // public function getPackagePlan()
    // {
    //     return $this;
    // }



















     /**
     * Set type
     *
     * @param string $type
     * @return PackagePlan
     */
     public function setType($type)
     {
         $this->type = $type;
 
         return $this;
     }

   
    /**
     * Get type
     *
     * @return string 
     */
     public function getType()
     {
         return $this->type;
     }
 
    





















     
     /**
     * Set amount
     *
     * @param string $amount
     * @return PackagePlan
     */
     public function setAmount($amount)
     {
         $this->amount = $amount;
 
         return $this;
     }


     
    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

   
 

   
  
    
    
    
   

    
   
   
    
   
}
