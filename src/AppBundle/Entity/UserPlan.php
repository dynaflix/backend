<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use MediaBundle\Entity\Media;
/**
 * Actor
 *
 * @ORM\Table(name="user_plan")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserPlanRepository")
 */
class UserPlan
{
   
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="username", type="string")
     */
    private $username;


     /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string")
     */
     private $name;


    /**
     * @var int
     * @ORM\Column(name="plantype", type="integer")
     */
    private $plantype;

    /**
     * @var string
     * @ORM\Column(name="amount", type="string", length=255)
     */
    private $amount	;



     /**
     * @var string
     * @ORM\Column(name="txnid", type="string", length=255)
     */
     private $txnid	;

         /**
     * @var string
     * @ORM\Column(name="orderid", type="string", length=255)
     */
      private $orderid	;



    /**
     * @var \DateTime
     * @ORM\Column(name="startdate	",type="datetime")
     */
     private $startdate	;

    /**
     * @var \DateTime
     * @ORM\Column(name="enddate	",type="datetime")
     */
     private $enddate	;



      /**
     * @var boolean
     * @ORM\Column(name="status	",type="boolean")
     */
     private $status	;






    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }




        /**
     * Set userid
     *
     * @param string $username
     * @return UserPlan
     */
     public function setUserName($username)
     {
         $this->username = $username;
 
         return $this;
     }
 
     /**
      * Get userid
      *
      * @return string 
      */
     public function getUserName()
     {
         return $this->username;
     }


            /**
     * Set userid
     *
     * @param string $name
     * @return UserPlan
     */
     public function setName($name)
     {
         $this->name = $name;
 
         return $this;
     }
 
     /**
      * Get name
      *
      * @return string 
      */
     public function getName()
     {
         return $this->name;
     }






     /**
     * Set plantype
     *
     * @param int $plantype
     * @return UserPlan
     */
     public function setPlanType($type)
     {
         $this->plantype = $type;
 
         return $this;
     }

   
    /**
     * Get plantype
     *
     * @return int 
     */
     public function getPlanType()
     {
         return $this->plantype;
     }
 

   

  
  
  
      
     /**
     * Set amount
     *
     * @param string $amount
     * @return UserPlan
     */
     public function setAmount($amount)
     {
         $this->amount = $amount;
 
         return $this;
     }


     
    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }





      
     /**
     * Set txnid
     *
     * @param string $txnid
     * @return UserPlan
     */
     public function setTxnId($txnid)
     {
         $this->txnid = $txnid;
 
         return $this;
     }


     
    /**
     * Get txnid
     *
     * @return string 
     */
    public function getTxnId()
    {
        return $this->txnid;
    }



       /**
     * Set orderid
     *
     * @param string $orderid
     * @return UserPlan
     */
     public function setOrderId($orderid)
     {
         $this->orderid = $orderid;
 
         return $this;
     }


     
    /**
     * Get orderid
     *
     * @return string 
     */
    public function getOrderId()
    {
        return $this->orderid;
    }














 /**
     * Set startdate
     *
     * @param \DateTime $startdate
     * @return $this
     */
     public function setStartDate($sdate)
     {
         $this->startdate = $sdate;
 
         return $this;
     }


     /**
     * Get startdate
     *
     * @return \DateTime 
     */
     public function getStartDate()
     {
         return $this->startdate;
     }




/**
     * Set enddate
     *
     * @param \DateTime $enddate
     * @return $this
     */
     public function setEndDate($edate)
     {
         $this->enddate = $edate;
 
         return $this;
     }


     /**
     * Get enddate
     *
     * @return \DateTime 
     */
     public function getEndDate()
     {
         return $this->enddate;
     }








/**
     * Set status
     *
     * @param boolean $status
     * @return $this
     */
     public function setStatus($sta)
     {
         $this->status = $sta;
 
         return $this;
     }


     /**
     * Get startdate
     *
     * @return boolean 
     */
     public function getStatus()
     {
         return $this->status;
     }





    





















  
   
 

   
  
    
    
    
   

    
   
   
    
   
}
