<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
class UserPlanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name','text',array( 'read_only' => true));
        $builder->add('plantype' ,ChoiceType::class, array(
            'choices' => array(
                "1" => "Daily",
                "30" => "Monthly",
                "365" => "Annualy",
               
            )));
        $builder->add('save', 'submit',array("label"=>"SAVE PLAN"));
       

    }
    public function getName()
    {
        return 'UserPlan';
    }
}
?>