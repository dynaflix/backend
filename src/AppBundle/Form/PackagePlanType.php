<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
class PackagePlanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');
        $builder->add('type' ,ChoiceType::class, array(
            'choices' => array(
                "Daily" => "Daily",
                "Monthly" => "Monthly",
                "Annualy" => "Annualy",
               
            )));
        $builder->add('amount');
        $builder->add('save', 'submit',array("label"=>"SAVE PLAN"));
       

    }
    public function getName()
    {
        return 'PackagePlan';
    }
}
?>